import { UserModel } from 'src/models';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserRegisterRequestDto {
  @ApiModelProperty()
  name: string;
  @ApiModelProperty()
  password: string;
  @ApiModelProperty()
  email: string;
}

export class UserRegisterResponseDto {
  @ApiModelProperty()
  user: UserModel;
}

export class UserLoginRequestDto {
  email: string;
  password: string;
}

export class UserLoginResponseDto {
  user: UserModel;
  token: string;
}

export class TokenPayloadDto {
  user: UserModel;
}
