import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';
import { UserService } from '../services';

@Injectable()
export class UserMiddleware implements NestMiddleware {

  constructor(private userService: UserService) { }

  resolve(...args: any[]): MiddlewareFunction {
    return (req, res, next) => {
      if (req.headers.token) {
        const payload = this.userService.tokenDecode(req.headers.token);
        if (payload) {
          req.user = payload;
        }
      }
      next();
    };
  }
}
