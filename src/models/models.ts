import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class UserModel {
  @ApiModelProperty()
  id: number;
  @ApiModelProperty()
  name: string;
  @ApiModelPropertyOptional({description: 'haslo powinno miec min 6 znakow, jedna cyfre i znak specjalny'})
  password?: string;
  @ApiModelPropertyOptional()
  email?: string;
  @ApiModelPropertyOptional()
  roles?: string[];
}

const user: UserModel = {
  id: 343,
  name: 'piotr',
  email: 'cos@do.pl',
};
